from torch.utils.data import Dataset
import torch
from utils import PocketFeaturizer, LigandFeaturizer
import os
import pandas as pd
import numpy as np

class LigandDataset(Dataset):
    def __init__(self, data_dir='./data/dude', max_deg=6,
                 seed=104398, max_num=None):
        self.seed = seed
        self.data_dir = data_dir
        self.max_deg = max_deg
        self.ligands = []

        ligand_fnames = [os.path.join(data_dir, dude_id, f) 
            for dude_id in os.listdir(data_dir)
            for f in os.listdir(os.path.join(data_dir, dude_id))
            if '.ism' in f]

        for ligand_fname in ligand_fnames:
            with open(ligand_fname, 'r') as f:
                self.ligands.extend(
                    [line.split(' ')[0] for line in f.readlines()])

        self.ligands = list(set(self.ligands))

        if max_num:
            np.random.seed(seed)
            np.random.shuffle(self.ligands)
            self.ligands = self.ligands[:max_num]

    def __getitem__(self, index):
        ligand_smiles = self.ligands[index]

        featurizer = LigandFeaturizer(ligand_smiles=ligand_smiles)

        node_feature = featurizer.get_node_features()
        adjacency_list = featurizer.get_adjacency_list(max_deg=self.max_deg)

        return torch.Tensor(node_feature), torch.Tensor(adjacency_list)

    def __nfeats__(self):
        return self.__getitem__(0)[0].size()[1]

    def __len__(self):
        return len(self.ligands)


class PDBBindPocketDataset(Dataset):
    def __init__(self, index_csv = './data/pdbbind_binding_affinity.csv',
                 data_dir='./data/pdbbind/v2018', max_deg=20):
        index_df = pd.read_csv(index_csv)
        self.data_dir = data_dir
        self.max_deg = max_deg
        self.pdbids = index_df.id.tolist()

    def __getitem__(self, index):
        pdbid = self.pdbids[index]

        protein_pdb_file = os.path.join(
            self.data_dir, pdbid, '%s_protein.pdb' % pdbid)
        pocket_pdb_file = os.path.join(
            self.data_dir, pdbid, '%s_pocket.pdb' % pdbid)
        featurizer = PocketFeaturizer(
            protein_pdb_file=protein_pdb_file,
            pocket_pdb_file=pocket_pdb_file)

        node_feature = featurizer.get_node_features()
        adjacency_list = featurizer.get_adjacency_list(max_deg=self.max_deg)

        return torch.Tensor(node_feature), torch.Tensor(adjacency_list)

    def __len__(self):
        return len(self.pdbids)

    def __nfeats__(self):
        return self.__getitem__(0)[0].size()[1]


class DUDEDataset(Dataset):
    def __init__(self, seed=128,
                 pocket_max_deg=20,
                 ligand_max_deg=6,
                 num_target=99999,
                 train_test_split=0.05, 
                 index_csv='./data/dude2pdb.csv', 
                 data_dir='./data'):
        index_df = pd.read_csv(index_csv)
        self.seed = seed
        self.dude_dir = os.path.join(data_dir, 'dude')
        self.pdbbind_dir = os.path.join(data_dir, 'pdbbind', 'v2018')

        self.pocket_max_deg = pocket_max_deg
        self.ligand_max_deg = ligand_max_deg

        np.random.seed(self.seed)
        targets = index_df.id.unique().tolist()
        np.random.shuffle(targets)
        self.num_target = min(num_target, len(targets))

        targets = targets[:self.num_target]
        index_df = index_df[index_df.id.isin(targets)]
        index_df.index = range(index_df.shape[0])

        # Choose training / validation / test set since it is important
        # to separate targets for each set.
        # Downside is that the number of target-ligand combination of each
        # set can't be precisely determined.

        split = int(np.floor(train_test_split * len(targets)))
        self.train_targets, self.test_targets, self.val_targets = \
            targets[2*split:], targets[:split], targets[split:2*split]

        # Create the dataset. 
        # Choose decoys randomly according to the number of actives
        # Add dude_id for safety net
        id2dude = dict(zip(index_df.id.tolist(), index_df.dude_id.tolist()))
        index_df['ligand'] = [[] for _ in range(index_df.shape[0])]

        for i, row in index_df.iterrows():
            with open(
                os.path.join(data_dir, 'dude', row['dude_id'], 
                'actives_final.ism'), 'r') as f:
                actives = f.readlines()
            with open(
                os.path.join(data_dir, 'dude', row['dude_id'], 
                'decoys_final.ism'), 'r') as f:
                decoys = np.random.choice(
                    f.readlines(), row['num_actives'], replace=False)
            
            row['ligand'].extend([[active.strip(), 1] for active in actives])
            row['ligand'].extend([[decoy.strip(), 0] for decoy in decoys])

        self.data = pd.DataFrame(index_df.ligand.tolist(), index=index_df.id)\
            .stack().reset_index(level=1, drop=True).reset_index(
            name='ligand')[['ligand', 'id']]
        self.data['dude_id'] = self.data.id.apply(lambda x: id2dude.get(x, ''))
        self.data[['ligand', 'label']] = self.data['ligand'].apply(pd.Series)

    def __getitem__(self, index):
        pdbid = self.data.loc[index, 'id']
        dude_id = self.data.loc[index, 'dude_id']
        ligand_smiles = self.data.loc[index, 'ligand'].split(' ')[0]
        label = self.data.loc[index, 'label']

        # first search for files in pdbbind dataset
        protein_pdb_file = os.path.join(
            self.pdbbind_dir, pdbid, '%s_protein.pdb' % pdbid)
        pocket_pdb_file = os.path.join(
            self.pdbbind_dir, pdbid, '%s_pocket.pdb' % pdbid)

        # if not found, search for dude dataset
        if not os.path.isfile(protein_pdb_file) \
            or not os.path.isfile(pocket_pdb_file):
            protein_pdb_file = os.path.join(
                self.dude_dir, dude_id, 'receptor.pdb')
            pocket_pdb_file = os.path.join(
                self.dude_dir, dude_id, '%s_pocket.pdb' % pdbid)

        pocket_featurizer = PocketFeaturizer(
            protein_pdb_file=protein_pdb_file, 
            pocket_pdb_file=pocket_pdb_file)
        pocket_nodes = pocket_featurizer.get_node_features()
        pocket_adj_list = pocket_featurizer.get_adjacency_list(
            max_deg=self.pocket_max_deg)

        ligand_featurizer = LigandFeaturizer(ligand_smiles)
        ligand_nodes = ligand_featurizer.get_node_features()
        ligand_adj_list = ligand_featurizer.get_adjacency_list(
            max_deg=self.ligand_max_deg)

        return (torch.Tensor(pocket_nodes), torch.Tensor(pocket_adj_list)),\
               (torch.Tensor(ligand_nodes), torch.Tensor(ligand_adj_list)),\
               torch.Tensor([label]).view(-1).long()
        
    def __len__(self):
        return self.data.shape[0]

    def __train_test_split__(self):
        return (
            self.data[self.data.id.isin(self.train_targets)].index.tolist(),\
            self.data[self.data.id.isin(self.test_targets)].index.tolist(),\
            self.data[self.data.id.isin(self.val_targets)].index.tolist())

    def __nlabels__(self):
        return len(self.data.label.unique().tolist())
    
    def __nfeats__(self):
        sample_item = self.__getitem__(0)
        feat_pocket = sample_item[0][0].size()[1]
        feat_ligand = sample_item[1][0].size()[1]

        return feat_pocket, feat_ligand
    

class PDBBindDataset(Dataset):
    def __init__(self, seed=128,
                 pocket_max_deg=20,
                 ligand_max_deg=6,
                 num_target=99999,
                 train_test_split=0.05, 
                 index_csv='./data/pdbbind_binding_affinity.csv', 
                 data_dir='./data/pdbbind/v2018'):
        index_df = pd.read_csv(index_csv)
        self.seed = seed
        self.data_dir = data_dir

        self.pocket_max_deg = pocket_max_deg
        self.ligand_max_deg = ligand_max_deg

        np.random.seed(self.seed)
        targets = index_df.id.unique().tolist()
        np.random.shuffle(targets)
        self.num_target = min(num_target, len(targets))

        targets = targets[:self.num_target]
        self.data = index_df[index_df.id.isin(targets)]
        self.data.index = range(self.data.shape[0])

        split = int(np.floor(train_test_split * len(targets)))
        self.train_targets, self.test_targets, self.val_targets = \
            targets[2*split:], targets[:split], targets[split:2*split]


    def __getitem__(self, index):
        pdbid = self.data.loc[index, 'id']
        ligand_fnames = [
            os.path.join(self.data_dir, pdbid, pdbid + '_ligand' + ext)
            for ext in ['.mol2', '.pdb']]
        label = self.data.loc[index, 'binding_affinity']

        protein_pdb_file = os.path.join(
            self.data_dir, pdbid, '%s_protein.pdb' % pdbid)
        pocket_pdb_file = os.path.join(
            self.data_dir, pdbid, '%s_pocket.pdb' % pdbid)
        pocket_featurizer = PocketFeaturizer(
            protein_pdb_file=protein_pdb_file,
            pocket_pdb_file=pocket_pdb_file)
        
        pocket_nodes = pocket_featurizer.get_node_features()
        pocket_adj_list = pocket_featurizer.get_adjacency_list(
            max_deg=self.pocket_max_deg)

        ligand_featurizer = LigandFeaturizer(
            ligand_smiles='', ligand_fname=ligand_fnames[0])
        if not ligand_featurizer.is_valid():
            ligand_featurizer = LigandFeaturizer(
            ligand_smiles='', ligand_fname=ligand_fnames[1])

        if not ligand_featurizer.is_valid():
            return None
        
        ligand_nodes = ligand_featurizer.get_node_features()
        ligand_adj_list = ligand_featurizer.get_adjacency_list(
            max_deg=self.ligand_max_deg)

        return (torch.Tensor(pocket_nodes), torch.Tensor(pocket_adj_list)),\
               (torch.Tensor(ligand_nodes), torch.Tensor(ligand_adj_list)),\
               torch.Tensor([label]).view(-1).float()
        
    def __len__(self):
        return self.data.shape[0]

    def __train_test_split__(self):
        return (
            self.data[self.data.id.isin(self.train_targets)].index.tolist(),\
            self.data[self.data.id.isin(self.test_targets)].index.tolist(),\
            self.data[self.data.id.isin(self.val_targets)].index.tolist())

    def __nlabels__(self):
        return 1
    
    def __nfeats__(self):
        sample_item = self.__getitem__(0)
        feat_pocket = sample_item[0][0].size()[1]
        feat_ligand = sample_item[1][0].size()[1]

        return feat_pocket, feat_ligand