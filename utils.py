import mdtraj as md
import numpy as np 
import pandas as pd
from scipy.spatial.distance import cdist
import os
from itertools import groupby
from operator import itemgetter
from rdkit import Chem
from rdkit.Chem import rdmolfiles
from rdkit.Chem import MolFromSmiles

_DSSP = ['H', 'B', 'E', 'G', 'I', 'T', 'S', ' ', 'NA']
_ATOMS = ['H', 'C', 'N', 'O', 'S', 'UNK']
_NUM_BONDS = [0, 1, 2, 3, 4, 5, 'UNK']
_AMINO_ACIDS = ['ALA', 'ARG', 'ASN', 'ASP', 'CYS', 'GLU', 'GLN', 'GLY', 'HIS',
    'ILE', 'LEU', 'LYS', 'MET', 'PHE', 'PRO', 'SER', 'THR', 'TRP', 'TYR', 'VAL']
_LIGAND_ATOMS =  ['C', 'N', 'O', 'S', 'F', 'Si', 'P', 'Cl', 'Br', 'Mg',
    'Na', 'Ca', 'Fe', 'As', 'Al', 'I', 'B', 'V', 'K', 'Tl', 'Yb', 'Sb', 'Sn',
    'Ag', 'Pd', 'Co', 'Se', 'Ti', 'Zn', 'H', 'Li', 'Ge', 'Cu', 'Au', 'Ni',
    'Cd', 'In', 'Mn', 'Zr', 'Cr', 'Pt', 'Hg', 'Pb', 'As', 'UNK']
_NUM_HYDROGENS = [0, 1, 2, 3, 4, 'UNK']
_VALENCE = [0, 1, 2, 3, 4, 5, 6, 'UNK']
_FORMAL_CHARGE = [-3, -2, -1, 0, 1, 2, 3, 'UNK']
#_possible_degree_list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
_HYBRIDIZATION = [
    Chem.rdchem.HybridizationType.SP, Chem.rdchem.HybridizationType.SP2,
    Chem.rdchem.HybridizationType.SP3, Chem.rdchem.HybridizationType.SP3D,
    Chem.rdchem.HybridizationType.SP3D2, 'UNK']
_RADICAL_E = [0, 1, 2, 'UNK']
_CHIRALITY = ['R', 'S', 'UNK']


def one_hot_encoding_unk(x, set):
    """Maps inputs not in the allowable set to the last element."""
    if x not in set:
        x = set[-1]
    return list(map(lambda s: int(x == s), set))


def pad_adjacency_list(l, max_deg, pad=-1):
    if len(l) > max_deg: return l[:max_deg]
    else: return l + [pad] * (max_deg - len(l))


def mol_from_file(fname):
    if fname.endswith('.mol2'): return rdmolfiles.MolFromMol2File(fname)
    elif fname.endswith('.pdb'): return rdmolfiles.MolFromPDBFile(fname)
    else:
        raise ValueError('File extension {} not supported'.format(
            fname.split('.')[-1]))


class GraphFeaturizer(object):
    def __init__(self):
        self.nodes = []
        self.node_features = None
        self.adjacency_list = None

    def get_node_features(self):
        raise NotImplementedError()

    def get_adjacency_list(self):
        raise NotImplementedError()


class PocketFeaturizer(GraphFeaturizer):
    def __init__(self, protein_pdb_file, pocket_pdb_file,
                distance_threshold=0.6, max_num_atoms_per_res=24):
        self.distance_threshold = distance_threshold
        self.max_num_atoms_per_res = max_num_atoms_per_res

        self.protein_md = md.load(protein_pdb_file)
        # self.protein_mol = rdmolfiles.MolFromPDBFile(
        #     os.path.join(data_dir, self.pdbid, "{}_protein.pdb".format(pdbid)))

        self.protein_table, _ = self.protein_md.topology.to_dataframe()
        self.protein_table = self.protein_table[self.protein_table.element != 'H']
        self.protein_table['resId'] = self.protein_table['resName'] + \
            self.protein_table['resSeq'].map(str)

        pocket_md = md.load(pocket_pdb_file)
        pocket_table, _ = pocket_md.topology.to_dataframe()
        pocket_table = pocket_table[pocket_table.element != 'H']

        # nodes are the residue name + seqid
        self.residues = {str(r): r for r in pocket_md.topology.residues
                        if r.name in _AMINO_ACIDS}
        self.nodes = list(self.residues.keys())
        self.node2index = {node: i for i, node in enumerate(self.nodes)}

        self.pocket_table = self.protein_table[
            self.protein_table.resId.isin(self.nodes)]

        self.amino_acid_featurizer = AminoAcidFeaturizer()

    def get_node_features(self):
        """
        TODO Optimize it
        """
        node_features = [None] * len(self.nodes)

        for node in self.nodes:
            res = self.residues[node]
            feature = self.amino_acid_featurizer.featurize(res.name)
            # feature += [int(res.is_nucleic)]

            for i in range(self.max_num_atoms_per_res):
                if i >= res.n_atoms:
                    feature += [0] * 14
                else:
                    atom = res.atom(i)
                    feature += one_hot_encoding_unk(atom.element.name, _ATOMS)
                    feature += [int(atom.is_backbone)]
                    feature += one_hot_encoding_unk(atom.n_bonds, _NUM_BONDS)
            node_features[self.node2index[node]] = feature

        return np.array(node_features)

    def get_adjacency_list(self, max_deg=20):
        """
        Creates dictionary of adjacency list from the coordinates of atom 
            in the pocket residues.
        If the atoms in two different residues are closer than the threshold,
        the residues(nodes) are connected.
        """
        coords = np.squeeze(self.protein_md.xyz)

        atom2res = pd.Series(self.pocket_table.resId.values, 
            index=self.pocket_table.index).to_dict()
        atom_indices = list(atom2res.keys())
        atom_coords = coords[atom_indices, :]

        D = cdist(atom_coords, atom_coords)
        in_atoms, out_atoms = np.where(D < self.distance_threshold)
    
        edges = list(zip(
            [self.node2index[atom2res[atom_indices[i]]] for i in in_atoms], 
            [self.node2index[atom2res[atom_indices[i]]] for i in out_atoms]))
        edges = sorted(list(set(edges)))
        
        adjacency_list = {
            in_node: list(list(zip(*out_nodes))[1]) for in_node, out_nodes
            in groupby(edges, itemgetter(0))}

    
        self.adjacency_list = [
            pad_adjacency_list(out_nodes, max_deg=max_deg) for out_nodes
            in adjacency_list.values()]

        return np.array(self.adjacency_list)


class LigandFeaturizer:
    def __init__(self, ligand_smiles, ligand_fname=None):
        self.ligand = MolFromSmiles(ligand_smiles)
        if self.ligand.GetNumAtoms() == 0 \
            and os.path.isfile(ligand_fname):
            ligand = mol_from_file(ligand_fname)
            if ligand is not None:
                self.ligand = ligand

        self.atoms = {atom.GetIdx(): atom 
                      for atom in self.ligand.GetAtoms()}
        
        self.nodes = list(self.atoms.keys())
        self.node_features = None
        self.adjacency_list = None
    
    def get_node_features(self):
        node_features = [None] * len(self.atoms.keys())

        for atom_index in self.atoms:
            node_features[atom_index] = self.encode_atom(
                self.atoms[atom_index])
        
        return np.array(node_features)

    def get_adjacency_list(self, max_deg=6):
        self.adjacency_list = [[] for _ in range(len(self.nodes))]

        for bond in self.ligand.GetBonds():
            self.adjacency_list[
                bond.GetBeginAtomIdx()].append(bond.GetEndAtomIdx())
            self.adjacency_list[
                bond.GetEndAtomIdx()].append(bond.GetBeginAtomIdx())

        self.adjacency_list = [pad_adjacency_list(l, max_deg=max_deg)
            for l in self.adjacency_list]

        return self.adjacency_list

    def is_valid(self):
        return self.ligand.GetNumAtoms() > 0

    @staticmethod
    def encode_atom(atom):
        feature = one_hot_encoding_unk(atom.GetSymbol(), _LIGAND_ATOMS) + \
            one_hot_encoding_unk(atom.GetTotalNumHs(), _NUM_HYDROGENS) + \
            one_hot_encoding_unk(atom.GetImplicitValence(), _VALENCE) + \
            one_hot_encoding_unk(atom.GetHybridization(), _HYBRIDIZATION) + \
            one_hot_encoding_unk(atom.GetFormalCharge(), _FORMAL_CHARGE) + \
            [int(atom.GetIsAromatic())]
        
        return feature


class AminoAcidFeaturizer:
    def __init__(self, data_csv='./data/amino_acids.csv'):
        df = pd.read_csv(data_csv)

        self.data = df.set_index('3-letter').to_dict()

        self.all_amino_acids = list(self.data.keys())
        self.side_chain_class = df.side_chain_polarity.unique().tolist() \
            + ['UNK']
        self.side_chain_polarity = df.side_chain_polarity.unique().tolist() \
            + ['UNK']
        self.essence = df.essence.unique().tolist() + ['UNK']

    def featurize(self, code):
        feature_vec = one_hot_encoding_unk(code, self.all_amino_acids) + \
            one_hot_encoding_unk(
                self.data['side_chain_class'][code], self.side_chain_class) + \
            one_hot_encoding_unk(
                self.data['side_chain_polarity'][code], self.side_chain_polarity) + \
            one_hot_encoding_unk(
                self.data['essence'][code], self.essence) + \
            [self.data['pl'][code]] + [self.data['pK1'][code]] + \
            [self.data['pK2'][code]] + \
            [self.data['side_chain_charge_num'][code]]

        return feature_vec

