## Drug Target Interaction Prediction Model
Unofficial implementation of [Graph Convolutional Neural Networks for Predicting Drug-Target Interactions](https://pubs.acs.org/doi/abs/10.1021/acs.jcim.9b00628)


## How to get started ##

#### 1. Download Dataset & Preprocess it
- Download [Openbabel](http://openbabel.org/wiki/Category:Installation)
  - For Linux, run apt install.
  
```
sudo apt-get install openbabel
```
- Download and preprocess dataset (PDBBind, DUD-E).
- **Warning** This will take about 9Gb of your space.  

```
./download.sh
```

#### 2. Create conda environment and install dependencies
WARNING: RDKit seems to crash when pytorch is installed with pip ([reference](https://github.com/molecularsets/moses/issues/40)). Download pytorch with conda to resolve this issue.

```
conda create -c rdkit -n {NAME} rdkit
conda activate {NAME}
conda install -c pytorch pytorch==1.2.0
pip install -r requirements.txt
```

#### 3. Train Autoencoder.
```
python train_autoencoder.py --log_dir ./logs/autoencoder
tensorboard --logdir=logs/autoencoder
```

When the loss converges, save the best model in the `./model/autoencoder`

```
mv ./model/{BEST_EPOCH}.pkl ./model/autoencoder/
```

#### 4. Train prediction model.
```
python train.py --pretrained_encoder_file ./model/autoencoder/{AUTOENCODER_NAME}.pkl
tensorboard --logdir=logs
```
