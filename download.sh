wget http://dude.docking.org/db/subsets/all/all.tar.gz -P ./data

mkdir ./data/dude
tar -xzvf ./data/all.tar.gz -C ./data/dude
mv ./data/dude/all/* ./data/dude
rm -rf ./data/dude/all

for d in ./data/dude/*/; do
    rm $d/*.mol2.gz
    rm $d/*.sdf.gz
done

rm -rf ./data/all.tar.gz

echo "Done downloading DUD-E"
# Downloading PDBBind (both general and refined set)
wget http://pdbbind.org.cn/download/pdbbind_v2018_other_PL.tar.gz -P ./data
wget http://pdbbind.org.cn/download/pdbbind_v2018_refined.tar.gz -P ./data

# Unzipping the data
mkdir ./data/pdbbind
tar -xzvf ./data/pdbbind_v2018_other_PL.tar.gz -C ./data/pdbbind
tar -xzvf ./data/pdbbind_v2018_refined.tar.gz -C ./data/pdbbind

mv ./data/pdbbind/refined-set/index ./data/pdbbind/v2018-other-PL/refined-index
mv ./data/pdbbind/refined-set/readme ./data/pdbbind/v2018-other-PL/refined-readme
mv ./data/pdbbind/refined-set/* ./data/pdbbind/v2018-other-PL/
mv ./data/pdbbind/v2018-other-PL/ ./data/pdbbind/v2018


# Using openbabel, convert .sdf to .pdb
# Remove water from .pdb
# Remove unused files
cd ./data/pdbbind/v2018
for pdbid in *; do
    if [ ! -f "${pdbid}"/"${pdbid}"_protein.pdb ]
    then
        echo "Skipping ${pdbid} folder"
        continue
    else
        echo ${pdbid}
        obabel ${pdbid}/${pdbid}_ligand.sdf -xr -O ${pdbid}/${pdbid}_ligand.pdb > /dev/null 2>&1
        mv ${pdbid}/${pdbid}_protein.pdb  ${pdbid}/${pdbid}_protein_hoh.pdb
        grep -v 'HOH' ${pdbid}/${pdbid}_protein_hoh.pdb >  ${pdbid}/${pdbid}_protein.pdb
        mv ${pdbid}/${pdbid}_pocket.pdb  ${pdbid}/${pdbid}_pocket_hoh.pdb
        grep -v 'HOH' ${pdbid}/${pdbid}_pocket_hoh.pdb >  ${pdbid}/${pdbid}_pocket.pdb
        mv ${pdbid}/${pdbid}_ligand.pdb  ${pdbid}/${pdbid}_ligand_hoh.pdb
        grep -v 'HOH' ${pdbid}/${pdbid}_ligand_hoh.pdb >  ${pdbid}/${pdbid}_ligand.pdb
        rm ${pdbid}/${pdbid}_ligand_hoh.pdb  ${pdbid}/${pdbid}_protein_hoh.pdb ${pdbid}/${pdbid}_pocket_hoh.pdb
        rm ${pdbid}/${pdbid}_ligand.sdf
    fi
done

cd ../../../
rm -rf ./data/pdbbind/refined-set
rm ./data/pdbbind_v2018_other_PL.tar.gz
rm ./data/pdbbind_v2018_refined.tar.gz

echo "Done downloading data!"
