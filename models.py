from layers import *
import torch
import torch.nn as nn
import torch.nn.functional as F

class DTIPredictionModel(nn.Module):
    def __init__(self, target_graph_encoder, ligand_graph_encoder,
                 feat_target, feat_ligand, dropout, device,
                 n_filters_target=[200, 100], n_filters_ligand=[200, 100],
                 n_fp_target=512, n_fp_ligand=216,
                 max_deg_target=20, max_deg_ligand=6,
                 n_linear=100, n_out=2, mode='classification'):
        super(DTIPredictionModel, self).__init__()

        self.mode = mode
        self.device = device

        self.dropout = nn.Dropout(p=dropout)
        self.target_graph_encoder = GraphAutoEncoder(
            n_features=feat_target, n_filters=n_filters_target,
            n_fp=n_fp_target, max_deg=max_deg_target).to(self.device)
        self.target_graph_encoder.load_state_dict(
            torch.load(target_graph_encoder, map_location=self.device))

        self.ligand_graph_encoder = GraphAutoEncoder(
            n_features=feat_ligand, n_filters=n_filters_ligand,
            n_fp=n_fp_ligand, max_deg=max_deg_ligand).to(self.device)
        self.ligand_graph_encoder.load_state_dict(
            torch.load(ligand_graph_encoder, map_location=self.device))

        self.linear = nn.Linear(n_fp_target + n_fp_ligand, n_linear)
        #self.batch_norm = nn.BatchNorm1d(n_fp_target + n_fp_ligand)

        if self.mode == 'regression':
            #self.batch_norm_2 = nn.BatchNorm1d(n_linear)

            self.linear_2 = nn.Linear(n_linear, n_linear)
            self.linear_3 = nn.Linear(n_linear, n_linear)

        self.out = nn.Linear(n_linear, n_out)

    def forward(self, X_target, adj_list_target, X_ligand, adj_list_ligand):
        X_target = self.target_graph_encoder.encode(X_target, adj_list_target)
        X_ligand = self.ligand_graph_encoder.encode(X_ligand, adj_list_ligand)

        X_target = torch.mean(X_target, axis=0).view(1, -1)
        X_ligand = torch.mean(X_ligand, axis=0).view(1, -1)

        X = torch.cat([X_target, X_ligand], axis=1)
        #X = self.batch_norm(X)
        X = F.relu(self.linear(X))
        X = self.dropout(X)

        if self.mode == 'regression':
            #X = self.batch_norm_2(self.linear_2(X))
            X = self.linear_2(X)
            X = self.dropout(F.relu(X))

            #X = self.batch_norm_2(self.linear_3(X))
            X = F.dropout(self.linear_3(X))

        return F.softmax(self.out(X))


class GraphAutoEncoder(nn.Module):
    def __init__(self, n_features=369, n_filters=[200, 100],
                 n_fp=512, max_deg=20):
        super(GraphAutoEncoder, self).__init__()
        self.neighborhood_enc_1 = NeighborhoodEncoder(
            n_features=n_features, n_hid=n_filters[0], max_deg=max_deg)
        self.fingerprint_1 = FingerPrintEncoder(
            n_features=n_filters[0], n_hid=n_fp)
        self.neighborhood_enc_2 = NeighborhoodEncoder(
            n_features=n_fp, n_hid=n_filters[1], max_deg=max_deg)
        self.fingerprint_2 = FingerPrintEncoder(
            n_features=n_filters[1], n_hid=n_fp)


    def forward(self, X, adj_list):
        X, N = self.neighborhood_enc_1.encode(X, adj_list)
        X = self.fingerprint_1.encode(X)
        X, _ = self.neighborhood_enc_2.encode(X, adj_list)
        X = self.fingerprint_2(X)
        X, _ = self.neighborhood_enc_2.decode(X, adj_list)
        X = self.fingerprint_1.decode(X)
        X, N_pred = self.neighborhood_enc_1.decode(X, adj_list)

        return X, N, N_pred

    def encode(self, X, adj_list):
        X, _ = self.neighborhood_enc_1.encode(X, adj_list)
        X = self.fingerprint_1.encode(X)
        X, _ = self.neighborhood_enc_2.encode(X, adj_list)
        X = self.fingerprint_2.encode(X)

        return X

"""
# This is the cleaner version but it doesn't work..
class GraphAutoEncoder(nn.Module):
    def __init__(self, n_features=369, n_filters=[200, 100],
                 n_fp=512, max_deg=20):
        super(GraphAutoEncoder, self).__init__()

        n_fps = [n_features] + [n_fp] * len(n_filters)

        self.gcn_layers = []
        self.fp_layers = []

        for i in range(len(n_filters)):
            self.gcn_layers.append(
                NeighborhoodEncoder(
                    n_features=n_fps[i], n_hid=n_filters[i], max_deg=max_deg))
            self.fp_layers.append(
                FingerPrintEncoder(
                    n_features=n_filters[i], n_hid=n_fps[i + 1]))

            self.add_module(
                'neighborhood_encoder_{}'.format(i), self.gcn_layers[i])
            self.add_module(
                'fingerprint_{}'.format(i), self.gcn_layers[i])


        if len(self.gcn_layers) != len(self.fp_layers):
            raise ValueError("There should be equal numbers of gcn layers" +
                             " and fingerprint layers")


    def forward(self, X, adj_list):

        # NOTE
        #    Neighborhood encoder and fingerprint encoder can easily be
        #    in one layer. However, they are implemented separately
        #    because paper clearly distinguishes them.

        X, N = self.encode(X, adj_list)
        X, N_pred = self.decode(X, adj_list)

        return X, N, N_pred

    def encode(self, X, adj_list):
        N = None
        for i in range(len(self.gcn_layers)):
            X, N_encoded = self.gcn_layers[i].encode(X, adj_list)
            X = self.fp_layers[i].encode(X)

            # Preserving the first neighborhood calculation
            # for loss calculation
            if i == 0:
                N = N_encoded.clone().detach()

        return X, N

    def decode(self, X, adj_list):
        for i in range(len(self.gcn_layers) - 1, -1, -1):
            X = self.fp_layers[i].decode(X)
            X, N_pred = self.gcn_layers[i].decode(X, adj_list)

        return X, N_pred
"""