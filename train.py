from __future__ import division
from __future__ import print_function

import os
import glob
import time
import random
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
from torch.utils.data.sampler import SubsetRandomSampler
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter

from data_utils import DUDEDataset, PDBBindDataset
from models import DTIPredictionModel
from hparams import Hparams
from loss_function import accuracy

hparams = Hparams()
parser = hparams.parser
args = parser.parse_args()
args.cuda = not args.no_cuda and torch.cuda.is_available()
device = torch.device('cuda' if args.cuda else 'cpu')

writer = SummaryWriter(log_dir=args.log_dir)

random.seed(args.seed)
np.random.seed(args.seed)
torch.manual_seed(args.seed)
if args.cuda:
    torch.cuda.manual_seed(args.seed)

# Load data
dataset = None
if args.mode == 'classification':
    dataset = DUDEDataset(
        seed=args.seed,
        train_test_split=args.train_test_split,
        num_target=args.num_target)
else:
    dataset = PDBBindDataset(
        seed=args.seed,
        train_test_split=args.train_test_split,
        num_target=args.num_target)

train_ind, test_ind, val_ind = dataset.__train_test_split__()
train_sampler = SubsetRandomSampler(train_ind)
val_sampler = SubsetRandomSampler(val_ind)
test_sampler = SubsetRandomSampler(test_ind)


train_loader = DataLoader(dataset,
                          batch_size=1,
                          sampler=train_sampler)
val_loader = DataLoader(dataset,
                        batch_size=1,
                        sampler=val_sampler)
test_loader = DataLoader(dataset,
                         batch_size=1,
                         sampler=test_sampler)

feat_target, feat_ligand = dataset.__nfeats__()
n_labels = dataset.__nlabels__()
if args.mode == 'regression':
    n_labels = 1

vars(args).update({'feat_target': feat_target,
                   'feat_ligand': feat_ligand,
                   'n_labels': n_labels,
                   'num_target': dataset.num_target})
hparam_fname = hparams.save_params(
    args, os.path.join(args.log_dir, 'hparams_{}.json'.format(time.time())))

model = DTIPredictionModel(
    target_graph_encoder=args.target_graph_encoder,
    ligand_graph_encoder=args.ligand_graph_encoder, dropout=args.dropout,
    feat_target=args.feat_target, feat_ligand=args.feat_ligand, device=device,
    n_filters_target=args.filter_size, n_filters_ligand=args.filter_size,
    n_fp_target=args.target_fingerprint, n_fp_ligand=args.ligand_fingerprint,
    max_deg_target=args.target_max_deg, max_deg_ligand=args.ligand_max_deg,
    n_linear=args.dim_linear, n_out=args.n_labels, mode=args.mode
)
model = model.to(device)

if args.pretrained_model_file != '':
    model.load_state_dict(
        torch.load(args.pretrained_model_file, map_location=device))

optimizer = optim.Adam(model.parameters(),
                       lr=args.lr,
                       weight_decay=args.weight_decay)
criterion = nn.CrossEntropyLoss()
if args.mode == 'regression':
    criterion = nn.MSELoss()



def train(epoch):
    t = time.time()
    model.train()

    losses_batch = []
    acc_batch = []
    for _ in range(args.batch_size):
        try:
            optimizer.zero_grad()

            (X_target, adj_list_target), (X_ligand, adj_list_ligand), label \
                = next(iter(train_loader))

            X_target, adj_list_target = \
                X_target.to(device), adj_list_target.to(device)
            X_ligand, adj_list_ligand = \
                X_ligand.to(device), adj_list_ligand.to(device)
            label = label.to(device)

            label_pred = model(
                X_target.squeeze(), adj_list_target.squeeze(),
                X_ligand.squeeze(), adj_list_ligand.squeeze())
            loss_train = criterion(label_pred, label.view(-1))
            losses_batch.append(loss_train)

            if args.mode == 'classification':
                acc_train = accuracy(label_pred, label.view(-1))
                acc_batch.append(acc_train)

            loss_train.backward()
            optimizer.step()
        except BaseException as e:
            print(e)
            pass

    avg_loss = torch.mean(torch.Tensor(losses_batch))
    avg_acc = torch.mean(torch.Tensor(acc_batch))
    writer.add_scalar('Training Loss', avg_loss.data.item(), epoch)
    writer.add_scalar('Training Accuracy', avg_acc.data.item(), epoch)

    print('Epoch: {:04d}'.format(epoch+1),
          'loss_train: {:.4f}'.format(avg_loss.data.item()),
          'acc_train: {:.4f}'.format(avg_acc.data.item()),
          'time: {:.4f}s'.format(time.time() - t))

    return avg_loss.data.item()


def evaluate():
    model.eval()

    losses_batch = []
    acc_batch = []
    for _ in range(args.batch_size):
        try:
            (X_target, adj_list_target), (X_ligand, adj_list_ligand), label \
                = next(iter(val_loader))

            X_target, adj_list_target = \
                X_target.to(device), adj_list_target.to(device)
            X_ligand, adj_list_ligand = \
                X_ligand.to(device), adj_list_ligand.to(device)
            label = label.to(device)

            label_pred = model(
                X_target.squeeze(), adj_list_target.squeeze(),
                X_ligand.squeeze(), adj_list_ligand.squeeze())
            loss_val = criterion(label_pred, label.view(-1))
            losses_batch.append(loss_val)

            if args.mode == 'classification':
                acc_val = accuracy(label_pred, label.view(-1))
                acc_batch.append(acc_val)
        except:
            pass

    avg_loss = torch.mean(torch.Tensor(losses_batch))
    avg_acc = torch.mean(torch.Tensor(acc_batch))
    writer.add_scalar('Validation Loss', avg_loss.data.item(), epoch)
    writer.add_scalar('Validation Accuracy', avg_acc.data.item(), epoch)

    print('Epoch: {:04d}'.format(epoch+1),
          'loss_val: {:.4f}'.format(avg_loss.data.item()),
          'acc_val: {:.4f}'.format(avg_acc.data.item()))

    return avg_loss


def compute_test():
    model.eval()

    losses_batch = []
    acc_batch = []
    for _ in range(len(test_loader)):
        try:
            (X_target, adj_list_target), (X_ligand, adj_list_ligand), label \
                = next(iter(test_loader))

            X_target, adj_list_target = \
                X_target.to(device), adj_list_target.to(device)
            X_ligand, adj_list_ligand = \
                X_ligand.to(device), adj_list_ligand.to(device)
            label = label.to(device)

            label_pred = model(
                X_target.squeeze(), adj_list_target.squeeze(),
                X_ligand.squeeze(), adj_list_ligand.squeeze())
            loss_test = criterion(label_pred, label.view(-1))
            losses_batch.append(loss_test)

            if args.mode == 'classification':
                acc_test = accuracy(label_pred, label.view(-1))
                acc_batch.append(acc_test)
        except:
            pass

    avg_loss = torch.mean(torch.Tensor(losses_batch))
    avg_acc = torch.mean(torch.Tensor(acc_batch))


    print('Epoch: {:04d}'.format(epoch+1),
          'loss_test: {:.4f}'.format(avg_loss.data.item()),
          'acc_test: {:.4f}'.format(avg_acc.data.item()))

    return avg_loss


# Train model
t_total = time.time()
loss_values = []
bad_counter = 0
best = args.epochs + 1
best_epoch = 0
for epoch in range(args.epochs):
    #try:
    loss_values.append(train(epoch))

    if epoch % args.validate_every_n_epochs == 0:
        try:
            evaluate()
        except:
            pass
        torch.save(model.state_dict(),
        '{}/{}.pkl'.format(args.model_dir, epoch))
    #except BaseException as e:
    #    print(e)
    #    pass
"""
    if loss_values[-1] < best:
        best = loss_values[-1]
        best_epoch = epoch
        bad_counter = 0
    else:
        bad_counter += 1


files = glob.glob('{}/*.pkl'.format(args.model_dir))
for file in files:
    epoch_nb = int(file.split('.')[0])
    if epoch_nb < best_epoch:
        os.remove(file)
"""

print("Optimization Finished!")
print("Total time elapsed: {:.4f}s".format(time.time() - t_total))

# Restore best model
last_epoch = max([int(os.path.basename(file).split('.')[0])
                 for file in os.listdir(args.model_dir)
                 if file.endswith('.pkl')])
print('Loading {}th epoch'.format(best_epoch))
model.load_state_dict(
    torch.load('{}/{}.pkl'.format(args.model_dir, last_epoch)))
compute_test()
# Testing
writer.close()