import torch
import torch.nn as nn
import torch.nn.functional as F


class NeighborhoodEncoder(nn.Module):
    def __init__(self, n_features, n_hid, max_deg):
        super(NeighborhoodEncoder, self).__init__()

        self.n_features = n_features
        self.n_hid = n_hid

        self.W_degs = nn.ParameterList(
            [nn.Parameter(torch.zeros(size=(n_hid, n_features)))
            for _ in range(max_deg)])
        self.W_self = nn.Parameter(torch.zeros(size=(n_hid, n_features)))
        self.b_encoder = nn.Parameter(torch.zeros(size=(1,)))
        self.bs_decoder = nn.ParameterList(
            [nn.Parameter(torch.zeros(size=(n_features, 1))) 
            for _ in range(2)])
        self.activation_fn = nn.ReLU()

        for i in range(len(self.W_degs)):
            nn.init.xavier_uniform_(self.W_degs[i].data, gain=1.414)

        nn.init.xavier_uniform_(self.W_self, gain=1.414)

        for i in range(len(self.bs_decoder)):
            nn.init.xavier_uniform_(self.bs_decoder[i].data, gain=1.414)

    def forward(self, X, adj_lists):
        X_out, X_n = self.encode(X, adj_lists)
        X_decode, X_n_decode = self.decode(X_out, adj_lists)

        return X_decode, X_n_decode, X_n

    def encode(self, X, adj_lists):
        N = X.size()[0]
        X_out = []
        X_n = []
        # for each node X_i
        for i in range(X.size()[0]):

            neighbor = adj_lists[i, :][adj_lists[i, :] > -1].long()
            deg_i = neighbor.size()[0]
            X_n_i = torch.sum(X[neighbor, :], axis=0).float().view(-1, 1) / deg_i

            h_n = torch.matmul(self.W_degs[deg_i-1], X_n_i)
            h_self = torch.matmul(self.W_self, X[i, :].view(-1, 1))

            X_i = self.activation_fn(h_n + h_self + self.b_encoder)
            X_out.append(X_i.view(1, -1))
            X_n.append(X_n_i.view(1, -1))

        X_out = torch.cat(X_out)
        X_n = torch.cat(X_n)

        return X_out, X_n

    def decode(self, X, adj_lists):
        N = X.size()[0]
        X_n = []
        X_out = []

        for i in range(X.size()[0]):
            neighbor = adj_lists[i, :][adj_lists[i, :] > -1].long()
            deg_i = neighbor.size()[0]

            X_n_i = self.activation_fn(torch.mm(
                self.W_degs[deg_i-1].transpose(0, 1), X[i, :].view(-1, 1)) + \
                self.bs_decoder[0])
            X_i = self.activation_fn(torch.mm(
                self.W_self.transpose(0, 1), X[i, :].view(-1, 1)) + \
                self.bs_decoder[1])

            X_n.append(X_n_i.view(1, -1))
            X_out.append(X_i.view(1, -1))

        X_n = torch.cat(X_n)
        X_out = torch.cat(X_out)

        return X_out.squeeze(), X_n.squeeze()


class FingerPrintEncoder(nn.Module):
    def __init__(self, n_features, n_hid):
        super(FingerPrintEncoder, self).__init__()

        self.W = nn.Parameter(torch.zeros(size=(n_features, n_hid)))
        self.b_encoder = nn.Parameter(torch.zeros(size=(1, n_hid)))
        self.b_decoder = nn.Parameter(torch.zeros(size=(1, n_features)))

    def forward(self, X):
        return self.decode(self.encode(X))

    def encode(self, X):
        return F.softmax(torch.matmul(X, self.W) + self.b_encoder)

    def decode(self, X):
        return F.tanh(torch.mm(X, self.W.transpose(0, 1)) + self.b_decoder)
