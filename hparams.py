import argparse
import json

class Hparams:
    parser = argparse.ArgumentParser()

    parser.add_argument('--no-cuda', action='store_true', default=False,
        help='Disables CUDA training.')
    parser.add_argument('--seed', type=int, default=72,
        help='Random seed.')
    parser.add_argument('--epochs', type=int, default=10000,
        help='Number of epochs to train.')
    parser.add_argument('--validate_every_n_epochs', type=int, default=20,
        help='Validate and save model every n epochs.')
    parser.add_argument('--batch_size', type=int, default=10,
        help='Batch size for training / testing')
    parser.add_argument('--lr', type=float, default=0.0005,
        help='Initial learning rate.')
    parser.add_argument('--weight_decay', type=float, default=0.0,
        help='Weight decay (L2 loss on parameters).')
    parser.add_argument('--dropout', type=float, default=0.3,
        help='Weight decay (L2 loss on parameters).')

    parser.add_argument('--num_target', type=int, default=99999,
        help='Number of target molecules to train')
    parser.add_argument('--filter_size', nargs='+', default=[200, 100],
        help='Size of the graph convolution filters')
    parser.add_argument('--target_fingerprint', type=int, default=512,
        help='Size of the target fingerprint')
    parser.add_argument('--ligand_fingerprint', type=int, default=216,
        help='Size of the ligand fingerprint')
    parser.add_argument('--target_max_deg', type=int, default=20,
        help='Maximum degree in adjacency list for target')
    parser.add_argument('--ligand_max_deg', type=int, default=6,
        help='Maximum degree in adjacency list for ligand')
    parser.add_argument('--dim_linear', type=int, default=100,
        help='Size of the linear layer for classification')
    parser.add_argument('--n_labels', type=int, default=2,
        help='Number of target molecules to train')
    parser.add_argument('--train_ligand_autoencoder', action='store_true',
        default=False, help='For autoencoder training only.')

    parser.add_argument('--train_test_split', type=float, default=0.05,
        help='Train-test split ratio')
    parser.add_argument('--pretrained_model_file', type=str,
        default='', help='Pretrained baseline model')
    parser.add_argument('--target_graph_encoder', type=str,
        default='./model/autoencoder/target_baseline.pkl',
        help='Pretrained autoencoder for target')
    parser.add_argument('--ligand_graph_encoder', type=str,
        default='./model/autoencoder/ligand_baseline.pkl',
        help='Pretrained autoencoder for ligand')
    parser.add_argument('--model_dir', type=str, default='./model',
        help='Directory to save model')
    parser.add_argument('--log_dir', type=str, default='./logs',
        help='Directory to save logs')
    parser.add_argument('--mode', type=str, default='classification',
        choices=['classification', 'regression'], help="Training mode")

    def save_params(self, args, fname):
        with open(fname, 'w') as f:
            json.dump(vars(args), f, indent=4)

        print('Saving parameters to ' + fname)

        return fname

    def load_params(self, fname):
        parser = argparse.ArgumentParser()
        with open(fname, 'r') as f:
            params = json.loads(f.read())

        for name, value in params.items():
            parser.add_argument(
                '--{}'.format(name), type=type(value), default=value)

        args = parser.parse_known_args()[0]

        return args
