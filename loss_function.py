import torch
import torch.nn as nn

class AutoEncoderLoss(nn.Module):
    def __init__(self):
        super(AutoEncoderLoss, self).__init__()

    def forward(self, X_pred, X, N_pred, N):
        self_loss = nn.MSELoss()(X_pred, X)
        neighbor_loss = nn.MSELoss()(N_pred, N)

        return self_loss.sum() + neighbor_loss.sum()

def accuracy(output, labels):
    preds = output.max(1)[1].type_as(labels)
    correct = preds.eq(labels).double()
    correct = correct.sum()
    return correct / len(labels)

