from __future__ import division
from __future__ import print_function

import os
import glob
import time
import random
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
from torch.utils.data.sampler import SubsetRandomSampler
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter

from data_utils import PDBBindPocketDataset, LigandDataset
from models import GraphAutoEncoder
from hparams import Hparams
from loss_function import AutoEncoderLoss

hparams = Hparams()
parser = hparams.parser
args = parser.parse_args()
args.cuda = not args.no_cuda and torch.cuda.is_available()

if args.cuda:
    try:
        torch.cuda.device(args.cuda_device)
    except:
        print('Using default CUDA device..')
        pass

writer = SummaryWriter(log_dir=args.log_dir)

random.seed(args.seed)
np.random.seed(args.seed)
torch.manual_seed(args.seed)
if args.cuda:
    torch.cuda.manual_seed(args.seed)

# Load data
dataset = PDBBindPocketDataset()
if args.train_ligand_autoencoder:
    dataset = LigandDataset()
    

dataset_size = len(dataset)
indices = list(range(dataset_size))
split = int(np.floor(args.train_test_split * dataset_size))
np.random.shuffle(indices)
train_indices, val_indices, test_indices \
    = indices[2*split:], indices[split:2*split], indices[:split]

train_sampler = SubsetRandomSampler(train_indices)
val_sampler = SubsetRandomSampler(val_indices)
test_sampler = SubsetRandomSampler(test_indices)


train_loader = DataLoader(dataset, 
                          batch_size=1,
                          sampler=train_sampler)
val_loader = DataLoader(dataset, 
                        batch_size=1,
                        sampler=val_sampler)
test_loader = DataLoader(dataset, 
                         batch_size=1,
                         sampler=test_sampler)

hparam_fname = hparams.save_params(
    args, os.path.join(args.log_dir, 'hparams_{}.json'.format(time.time())))

model = GraphAutoEncoder(
    n_features=dataset.__nfeats__(),
    n_filters=args.filter_size,
    n_fp=args.target_fingerprint,
    max_deg=args.target_max_deg)

if args.train_ligand_autoencoder:
    model = GraphAutoEncoder(
        n_features=dataset.__nfeats__(),
        n_filters=args.filter_size,
        n_fp=args.ligand_fingerprint, 
        max_deg=args.ligand_max_deg)

optimizer = optim.Adam(model.parameters(), 
                       lr=args.lr, 
                       weight_decay=args.weight_decay)
criterion = AutoEncoderLoss()

if args.cuda:
    model.cuda()


def train(epoch):
    t = time.time()
    model.train()
    optimizer.zero_grad()
    
    X, adj_lists = next(iter(train_loader))
    if args.cuda:
        X = X.cuda()
        adj_lists = adj_lists.cuda()

    X_pred, N, N_pred = model(
        X.squeeze(), adj_lists.squeeze())
    loss_train = criterion(X_pred, X.squeeze(), N_pred, N)

    writer.add_scalar('Training Loss', loss_train.data.item(), epoch)

    loss_train.backward()
    optimizer.step()

    print('Epoch: {:04d}'.format(epoch+1),
          'loss_train: {:.4f}'.format(loss_train.data.item()),
          'time: {:.4f}s'.format(time.time() - t))

    return loss_train.data.item()


def evaluate():
    model.eval()

    X, adj_lists = next(iter(val_loader))
    if args.cuda:
        X = X.cuda()
        adj_lists = adj_lists.cuda()

    X_pred, N, N_pred = model(
        X.squeeze(), adj_lists.squeeze())
    loss_val = criterion(X_pred, X.squeeze(), N_pred, N)

    writer.add_scalar('Validation Loss', loss_val.data.item(), epoch)


    print('Epoch: {:04d}'.format(epoch+1),
          'loss_val: {:.4f}'.format(loss_val.data.item()))


def compute_test():
    model.eval()

    X, adj_lists = next(iter(test_loader))
    if args.cuda:
        X = X.cuda()
        adj_lists = adj_lists.cuda()

    X_pred, N, N_pred = model(
        X.squeeze(), adj_lists.squeeze())
    loss_test = criterion(X_pred, X.squeeze(), N_pred, N)


    print('Epoch: {:04d}'.format(epoch+1),
          'loss_test: {:.4f}'.format(loss_test.data.item()))


# Train model
t_total = time.time()
loss_values = []
bad_counter = 0
best = args.epochs + 1
best_epoch = 0
for epoch in range(args.epochs):
    try:
        loss_values.append(train(epoch))

        if epoch % args.validate_every_n_epochs == 0:
            evaluate()
            torch.save(model.state_dict(), 
            '{}/{}.pkl'.format(args.model_dir, epoch))
    except BaseException as e:
        print(epoch)
        print(e)
"""
    if loss_values[-1] < best:
        best = loss_values[-1]
        best_epoch = epoch
        bad_counter = 0
    else:
        bad_counter += 1


files = glob.glob('{}/*.pkl'.format(args.model_dir))
for file in files:
    epoch_nb = int(file.split('.')[0])
    if epoch_nb < best_epoch:
        os.remove(file)
"""

print("Optimization Finished!")
print("Total time elapsed: {:.4f}s".format(time.time() - t_total))

# Restore best model
last_epoch = max([int(os.path.basename(file).split('.')[0]) 
                 for file in os.listdir(args.model_dir)
                 if file.endswith('.pkl')])
print('Loading {}th epoch'.format(last_epoch))
model.load_state_dict(
    torch.load('{}/{}.pkl'.format(args.model_dir, last_epoch)))
compute_test()
# Testing
writer.close()